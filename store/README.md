# Task

1. Інсталювати @angular/cli глобально і створити новий проект store.

2. Не додавати роутинг, стилями вибрати scss

3. Створити папку core(модуль), features, shared.

4. В core створити компоненти header, footer і сервіс authentication. Імпортувати core module в app module

5. Створити 2 feature модулі products i cart.

6. Всередині products створити компонент product, у якого changeDetection буде OnPush(використовуючи опції в cli)

7. В shared додати компонент button i input. Обидва компоненти мають бути з інлайновими стилями(не окремим файлом, а одразу в ts компоненті)

8. Використати button i input компоненті в products.

9. Використати input компонент в cart

Використовувати 1 модуль на 1 компонент(виключення - core module)

Зробити так, щоб html рендерився правильно при запуску через ng serve.
