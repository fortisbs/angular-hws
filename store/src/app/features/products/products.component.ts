import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  productsList = ['Product1', 'Product2', 'Product3']

  constructor() { }

  ngOnInit(): void {
  }

}
