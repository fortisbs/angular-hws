import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsComponent } from "./products.component";
import { ProductModule } from "./product/product.module";
import { InputModule } from "../../shared/input/input.module";
import { ButtonModule } from "../../shared/button/button.module";

@NgModule({
  declarations: [
    ProductsComponent
  ],
  imports: [
    CommonModule,
    ProductModule,
    InputModule,
    ButtonModule
  ],
  exports: [ProductsComponent]
})
export class ProductsModule { }
